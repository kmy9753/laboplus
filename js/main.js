﻿enchant();

/*

Core
- rootScene
- titleScene
-- Sprite ()
- storyScene
-- Sprite ()
- readyScene
- Sprite ()
- gameScene
-- Sprite ()
- gameOverScene
-- Sprite ()
- tutorialScene
-- Sprite ()

*/

var FPS = 30;
var END_TIME = 60.0;
var CELL_SIZE = 48;
var MAX_ROW = 5; // 縦のマス数
var MAX_COL = 6; // 横のマス数
var COLOR_VAL = 5; //色の数
var EXPRESSION_VAL = 1; //表情の数
var BORDER = 4;
var COMBO_TIME = FPS / 3;
var FALL_TIME = FPS / 3;
var DX = [-1, 0, 1, 0];
var DY = [0, -1, 0, 1];

var STORY_TEXT = ['〜〇月某日〜', 
'あなた「成績を見に来たけど、なんだこれは・・・・」', 
'幼馴染「やっほー！あれあなた君じゃん！久しぶり！！」', 
'あなた「昨日も会ったじゃないか・・・・」', 
'幼馴染「あれ？なんか元気ないね？どうしたの？」', 
'あなた「いや別に・・・ﾁﾗｯ」', 
'幼馴染「ﾌｰﾝ・・・ん？」', 
'あなた「あっ」', 
'幼馴染「この成績・・・どうしたの！？」', 
'あなた「ううっ」', 
'幼馴染「これじゃ卒業も危ういじゃない！！」', 
'あなた「あうぅ」', 
'幼馴染「そんなぁ・・・これじゃ一緒に卒業できないかも・・・・」', 
'あなた「ｱﾍｯｱﾍｯ」', 
'天の声「お困りのようだな青年」', 
'あなた「なんだこの声は」', 
'天の声「まだ諦めてはいかん。求めよさらば与えられん」', 
'あなた「でもどうやって・・・」', 
'天の声「君にこれから試練を与えよう。それをクリアすればお望みのものを与えよう」', 
'あなた「え！？本当ですか？？」', 
'天の声「そうじゃ　励めよ若者」',
'・・・'];
var TUTORIAL_TEXT = ['幼馴染「こんにちは」', '幼馴染「これからラボプラスの遊び方について説明するわ」', '幼馴染「あなたの目的は、『たくさん単位を取得して進級すること』よ」', '幼馴染「そこに並んでいるのが単位よ。外国語の単位や数学の単位、いろいろあるわね。」', '幼馴染「単位をなぞってみなさい」', '幼馴染「単位を消すことができたようね」', '幼馴染「同じ色の単位が4つ以上繋がると、単位を取得することができるわ」', '幼馴染「連続で単位を取得すると、コンボが繋がるわ」', '幼馴染「右上を見て」', '幼馴染「まずはスコアよ」', '幼馴染「スコアを大きくすることがあなたの使命よ」', '幼馴染「コンボ数・繋がる個数 が大きいほどスコアの上がり幅は大きくなるわ」', '幼馴染「次は制限時間よ」', '幼馴染「制限時間が0になるまでのスコアで結果が出るわ」', '幼馴染「3000点を超えると進級できるから、がんばってね」', '幼馴染「これで説明を終わるわ」', '幼馴染「好きなだけ練習していきなさい」'];
var CREDITS = ['実験', '化学', '物理', '数学', '外国語'];

var PUYO_IMG = 'img/puyo.png'
var START_IMG  = 'img/start.png';
var EXIT_IMG = 'img/exit.png';
var BOY_IMG = 'img/boy.png';
var GIRL_IMG = 'img/girl.png';
var LOGO_IMG = 'img/logo.png';
var TUTORIAL_IMG = 'img/tutorial.png';
var BOARD_IMG = 'img/board.png';
var LABO_PLUS_BG_IMG = 'img/laboplusBG.png';
var COMBO_EFFECT_IMG = 'img/comboEffect.png';
var GOD_LIGHT_IMG = 'img/godlight.png';
var STORY_BG_IMG = 'img/storyBG.png';
var RESULT_IMG = 'img/resultImg.png';

var TITLE_BGM = 'sound/titleBGM.mp3';
var STORY_BGM = 'sound/storyBGM.mp3';
var GAME_BGM = 'sound/gameBGM.mp3';
var PUSH_BUTTON_SE = 'sound/pushButtonSE.mp3';
var CHAIN_SE = 'sound/chainSE.mp3';

window.onload = function(){

    var core = new Core(320, 380);
    core.preload(PUYO_IMG, START_IMG, EXIT_IMG, GIRL_IMG, BOY_IMG, LOGO_IMG, TUTORIAL_IMG, BOARD_IMG, LABO_PLUS_BG_IMG, COMBO_EFFECT_IMG, GOD_LIGHT_IMG, STORY_BG_IMG, RESULT_IMG, TITLE_BGM, STORY_BGM, GAME_BGM, PUSH_BUTTON_SE, CHAIN_SE);
    core.fps = 15;
    
    var bgm;

    core.onload = function() {

        var Product = Class.create(Sprite, {
            initialize: function(width, height, path, x, y){
                Sprite.call(this, width, height);
                this.x = x;
                this.y = y;
                this.image = core.assets[path];
            }
        });

        var Human = Class.create(Product, {
            initialize: function(width, height, path, x, y){
                Product.call(this, width, height, path, x, y);

                //this.on('enterframe', function(){
                //    if(rand(FPS * 2) == 0)this.frame = rand(EXPRESSION_VAL);
                //});
            }
        });

        var Puyo = Class.create(Product, {

            initialize: function(x, y){
                Product.call(this, CELL_SIZE, CELL_SIZE, PUYO_IMG, x, y - 5);
                this.frame = rand(COLOR_VAL);
//                this.opacity = 0.0;
//                this.tl.fadeIn(core.fps / 3).moveBy(0, 5, core.fps / 3).and();
            }
        });

        var TextField = Class.create(Group, {
            
            initialize: function(text, width, height, x, y, font){
                Group.call(this);
                this.text = text;

                this.x = x;
                this.y = y;

                var bg = new Entity();
                bg.width = width;
                bg.height = height;
                bg.backgroundColor = "gray";
                bg.opacity = 0.3;

                var label = new Label();
                this.isEnd = false;
                this.textFrame = 0;
                label.text = this.text[this.textFrame];
                label.color = "black";
                if(font != undefined) label.font = font;
                label.moveTo(10, 10);
                label.width = width - 20;
                label.height = height - 20;
                label.opacity = 0.2;
                label.tl.fadeIn(FPS / 3);

                this.addChild(bg);
                this.addChild(label);

                this.on('touchstart', function(){
                    if(this.textFrame < text.length - 1){
                        label.tl.clear();
                        label.opacity = 0.2;
                        
                        this.textFrame++;
                        label.tl.fadeIn(FPS / 3);
                    }
                    if(this.textFrame >= text.length - 1){
                        this.isEnd = true;
                    }
                    label.text = this.text[this.textFrame];
                });
            }
        });

        function Result(){
            this.score = 0;
            this.maxCombo = 0;
            var puyoCnt = new Array(COLOR_VAL);
            for(var i = 0; i < COLOR_VAL; i++) puyoCnt[i] = 0;
            this.puyoCnt = puyoCnt;
        }

        Result.TEXT = ['退学になりました。', '留年しました。', '仮進級しました。', '進級しました。', '早期卒業しました。', '大学院に推薦で行きました', 'あなたは概念になりました'];
        Result.SCORE_PER_RANK = 1000;

        Result.prototype.getFrame = function(){
            var ind = Math.min(Math.floor(this.score / Result.SCORE_PER_RANK),
                               Result.TEXT.length - 1);

            return ind;
        }
        Result.prototype.getText = function(){
            return Result.TEXT[this.getFrame()];
        }

        var ResultViewer = Class.create(Group, {
            
            initialize: function(result, x, y){
                
                Group.call(this);

                this.moveTo(x, y);

                var w = 120;
                var h = 24;
                var curY = 0;
                var title = new TextField(["結果"], w, 2 * h, 0, curY, '24px "arial"');
                var gotCredit = new Array(COLOR_VAL);
                curY += 1.8 * h - h;
                for(var i = 0; i < COLOR_VAL; i++){
                    gotCredit[i] = new TextField([CREDITS[i] + ': ' + result.puyoCnt[i] + '単位'], 
                        w, h, 0, curY += h);
                }
                var maxCombo = new TextField(["最大コンボ: " + 
                    result.maxCombo], w, h, 0, curY += h);
                var score = new TextField(["スコア: " + result.score],
                    w, 1.2 * h, 0, curY += h);
                var judgement = new TextField([result.getText()], 
                    2 * w, 2 * h, 0, curY += 1.6 * h, '20px "arial"');

                var girl = new Product(150, 185, RESULT_IMG, w, 1.5 * h);
                girl.frame = result.getFrame();

                this.addChild(title);
                for(var i = 0; i < COLOR_VAL; i++) this.addChild(gotCredit[i]);
                this.addChild(maxCombo);
                this.addChild(score);
                this.addChild(judgement);
                this.addChild(girl);
            }
        });

        var Board = Class.create(Group, {

            initialize: function(x, y){

                Group.call(this);

                this.moveTo(x, y);

                this.puyoCnt = 0;
                this.comboCnt = 0;
                this.result = new Result();

                var bgObj = new Product(CELL_SIZE * MAX_COL, CELL_SIZE * MAX_ROW, BOARD_IMG);
                this.addChild(bgObj);

                var puyos = new Array(MAX_ROW * MAX_COL);
                //for(var y in puyos){
                for(var i = 0; i < MAX_ROW * MAX_COL; i++){
                    var obj = new Puyo(CELL_SIZE * (i % MAX_COL), CELL_SIZE * Math.floor(i / MAX_COL) - 5);
                    puyos[i] = obj;

                    this.addChild(puyos[i]);
                }

                var used = new Array(MAX_ROW);
                var existPuyo = new Array(MAX_ROW);
                var indexMap = new Array(MAX_ROW);

                for(var y = 0; y < MAX_ROW; y++){
                    used[y] = new Array(MAX_COL);
                    existPuyo[y] = new Array(MAX_COL);
                    indexMap[y] = new Array(MAX_COL);

                    for(var x = 0; x < MAX_COL; x++){
                        used[y][x] = false;
                        existPuyo[y][x] = true;
                        indexMap[y][x] = x + MAX_COL * y;
                    }
                }

                this.puyos = puyos;
                this.used = used;
                this.existPuyo = existPuyo;
                this.indexMap = indexMap;

                this.isRun = false;

                this.onFall();

                var onTouch = function(e){
                    if(this.isRun) return;

                    var xIndex = Math.floor((e.x - this.x) / CELL_SIZE);
                    var yIndex = Math.floor((e.y - this.y) / CELL_SIZE);

                    this.puyos[this.indexMap[yIndex][xIndex]].frame = 2 * COLOR_VAL;
                    this.existPuyo[yIndex][xIndex] = false;
                }

                this.on(enchant.Event.TOUCH_START, onTouch);
                this.on(enchant.Event.TOUCH_MOVE, onTouch);

                this.on(enchant.Event.TOUCH_END, function(){
                    if(this.isRun) return;
                    this.isRun = true;

                    var endFallTime = COMBO_TIME + FALL_TIME;
                    this.comboCnt = 0;
                    this.tl.loop();
                    this.tl.clear();
                    this.tl.cue({
                        10: function(){
                            this.rewriteMap();
                            this.onFall();
                        },
                        20: function(){
                            if(!this.chain()){
                                this.tl.unloop();
                                this.isRun = false;

                                this.result.maxCombo = 
                                    Math.max(this.result.maxCombo, this.comboCnt);
                            }
                        }
                    });
                });

            },

            rewriteMap: function(){
                for(var x = 0; x < MAX_COL; x++){
                    for(var y = 0; y < MAX_ROW; y++){
                        if(!this.existPuyo[y][x]){
                            var ind = this.indexMap[y][x];
                            for(var curY = y; 0 < curY; curY--){
                                this.indexMap[curY][x] = this.indexMap[curY - 1][x];
                            }
                            this.indexMap[curY][x] = ind;
                            this.puyos[ind].y = -CELL_SIZE / 2;
                            this.puyos[ind].frame = rand(COLOR_VAL);
                            this.existPuyo[y][x] = true;
                        }
                    }
                }
            },

            onFall: function(){
                for(var x = 0; x < MAX_COL; x++){
                    for(var y = 0; y < MAX_ROW; y++){
                        this.puyos[this.indexMap[y][x]].tl.show().moveTo(x * CELL_SIZE, y * CELL_SIZE, FALL_TIME, enchant.Easing.BACK_EASEOUT);
                    }
                }
            },

            chain: function(){

                for(var x = 0; x < MAX_COL; x++){
                    for(var y = 0; y < MAX_ROW; y++){
                        this.used[y][x] = false;
                        this.existPuyo[y][x] = true;
                    }
                }
                
                var existCombo = false;

                for(var x = 0; x < MAX_COL; x++){
                    for(var y = 0; y < MAX_ROW; y++){
                        if(!this.used[y][x]){
                            this.puyoCnt = 0;
                            var color = this.puyos[this.indexMap[y][x]].frame;
                            this.countPuyos(x, y, color);
                            if(this.puyoCnt >= BORDER){
                                core.assets[CHAIN_SE].clone().play();

                                this.comboCnt++;
                                this.result.puyoCnt[color] += this.puyoCnt;
                                
                                //var effect = new Product(100, 30, COMBO_EFFECT_IMG, CELL_SIZE * MAX_COL * 0.5, CELL_SIZE * MAX_ROW * 0.5);
                                var effect = new Product(100, 30, COMBO_EFFECT_IMG, CELL_SIZE * x, CELL_SIZE * y);
                                effect.frame = this.comboCnt - 1;
                                effect.tl.fadeOut(FPS).and().moveBy(0, -80, FPS).removeFromScene();
                                this.addChild(effect);

                                this.clearPuyos(x, y, color);
                                existCombo = true;
                                this.result.score += 1.5 * 8 * this.comboCnt * this.puyoCnt;
                            }
                        }
                    }
                }

                return existCombo;
            },

            countPuyos: function(x, y, color){

                if(x < 0 || MAX_COL <= x || 
                   y < 0 || MAX_ROW <= y) return;
                if(this.puyos[this.indexMap[y][x]].frame != color ||
                   this.used[y][x]) return;
                
                this.used[y][x] = true;
                this.puyoCnt++;
                
                for(var i = 0; i < 4; i++){
                    this.countPuyos(x + DX[i], y + DY[i], color);
                }
            },

            clearPuyos: function(x, y, color){

                if(x < 0 || MAX_COL <= x || 
                   y < 0 || MAX_ROW <= y) return;
                if(this.puyos[this.indexMap[y][x]].frame != color) return;
                
                this.existPuyo[y][x] = false;
                this.puyos[this.indexMap[y][x]].frame += COLOR_VAL;
                this.puyos[this.indexMap[y][x]].tl.fadeOut(COMBO_TIME);
                
                for(var i = 0; i < 4; i++){
                    this.clearPuyos(x + DX[i], y + DY[i], color);
                }
            }

        });
        
        
        /**
          * タイトルシーン
          */
        var createTitleScene = function() {
            var scene = new Scene();
            scene.backgroundColor = 'blue';

            var logo = new Product(178, 119, LOGO_IMG, 10, 50);
            logo.x = scene.width / 2 - logo.width / 2;

            var startButton = new Product(120, 50, START_IMG);
            startButton.moveTo(scene.width / 2 - startButton.width / 2,
                               scene.height - startButton.height * 3.5);

            var tutorialButton = new Product(120, 50, TUTORIAL_IMG);
            tutorialButton.moveTo(startButton.x,
                                 startButton.y + tutorialButton.height + 12);

            var girl = new Human(80, 185, GIRL_IMG);
            girl.moveTo(230, 150);

            var bgObj = new Product(scene.width, scene.height, LABO_PLUS_BG_IMG);
            scene.addChild(bgObj);

            if(bgm) bgm.stop();
            bgm = core.assets[TITLE_BGM];
            bgm.play();

            var thanks = new Label('画像: http://blog.goo.ne.jp/ari1192jp_001/<br>音楽: http://www.01earth.net/sound/<br>製作協力:@kurogomapurin_');
            thanks.font = "11px 'alial'";
            thanks.x = core.width - 200;
            thanks.y = core.height - 50;

            startButton.on('touchstart', function(){
                core.assets[PUSH_BUTTON_SE].clone().play();
                this.tl.fadeOut(FPS / 8).fadeIn(FPS / 10).then(function(){core.replaceScene(createStoryScene());});
            });

            tutorialButton.on('touchstart', function(){
                core.assets[PUSH_BUTTON_SE].clone().play();
                this.tl.fadeOut(FPS / 8).fadeIn(FPS / 10).then(function(){core.replaceScene(createTutorialScene());});
            });

            scene.on('enterframe', function(){
                if(bgm.currentTime == bgm.duration) bgm.play();
            });
//thimpo
            scene.addChild(logo);
            scene.addChild(girl);
            scene.addChild(startButton);
            scene.addChild(tutorialButton);
            scene.addChild(thanks);

            return scene;
        };

        //汎用exitボタン
        var exitButton = new Product(80, 50, EXIT_IMG);
        exitButton.moveTo(core.width - exitButton.width - 12,
                          core.height - exitButton.height - 12);

        exitButton.on('touchstart', function(){
            core.assets[PUSH_BUTTON_SE].clone().play();
            this.tl.fadeOut(FPS / 8).fadeIn(FPS / 10).then(function(){core.replaceScene(createTitleScene());});
        });

        /**
          * ストーリーシーン
          */
        var createStoryScene = function() {
            var scene = new Scene();
            scene.backgroundColor = 'green';

            var boy = new Human(80, 185, BOY_IMG);
            boy.moveTo(0, 60);
            boy.scale(-1, 1);
            boy.tl.moveTo(30, 70, 20);

            var girl = new Human(80, 185, GIRL_IMG);
            girl.moveTo(320, 60);

            var bg = new Product(scene.width, scene.height, STORY_BG_IMG);

            var textField = new TextField(STORY_TEXT, 300, 75, 10, 244);

            var godLight = new Product(270, 130, GOD_LIGHT_IMG);
            godLight.moveTo(scene.width / 2 - godLight.width / 2, -50);
            godLight.opacity = 0.0;

            bgm.stop();
            bgm = core.assets[STORY_BGM];
            bgm.play();

            scene.addChild(bg);
            scene.addChild(boy);
            scene.addChild(girl);
            scene.addChild(textField);
            scene.addChild(exitButton);

            girl.on('touchstart', function(){
                core.assets[PUSH_BUTTON_SE].clone().play();
                core.replaceScene(createReadyScene());
            });
            textField.on('touchstart', function(){
                core.assets[PUSH_BUTTON_SE].clone().play();

                if(textField.isEnd){
                    core.replaceScene(createReadyScene());
                }
                if(textField.textFrame == 13){
                    scene.addChild(godLight);
                    godLight.tl.moveTo(godLight.x, 0, FPS).and().waitUntil(function(){ this.opacity += 0.6 / FPS; return this.opacity >= 0.6 });
                }
                if(textField.textFrame == 1) girl.tl.moveTo(210, 70, 20);
            });

            scene.on('enterframe', function(){
                if(bgm.currentTime == bgm.duration) bgm.play();
            });

            return scene;
        };

        /**
          * レディーシーン
          */
        var createReadyScene = function() {
            var scene = new Scene();

            scene.backgroundColor = 'gray';

            bgm.stop();

            var textField = new Label();
            var bgObj = new Product(scene.width, scene.height, LABO_PLUS_BG_IMG);

            scene.addChild(bgObj);
            textField.font = '24px "arial"';
            textField.x = scene.width / 2 - 45;
            textField.y = 140;
            textField.text = "レディー？";

            scene.addChild(textField);

            scene.on('touchstart', function(){
                core.replaceScene(createGameScene());
            });

            return scene;
        };



        var timeField = new Label();
        timeField.font = '14px "arial"';
        timeField.x = core.width - 120;
        timeField.y = 12;

        var scoreField = new Label();
        scoreField.font = '18px "arial"';
        scoreField.x = timeField.x;
        scoreField.y = timeField.y + 20;

        /**
          * ゲームシーン
          */
        var createGameScene = function() {
            var scene = new Scene();
            scene.backgroundColor = 'gray';

            var remainingTime;
            var displayedScore = 0;

            core.frame = 0;

            var board = new Board(14, 60);

            var bgObj = new Product(scene.width, scene.height, LABO_PLUS_BG_IMG);
            scene.addChild(bgObj);

            //bgm.stop();
            bgm = core.assets[GAME_BGM];
            bgm.duration *= 2;
            bgm.play();

            scene.addChild(timeField);
            scene.addChild(scoreField);
            scene.addChild(exitButton);

            scene.addChild(board);

            scene.on('enterframe', function(){

                if(bgm.currentTime == bgm.duration) bgm.play();

                if(remainingTime <= 0) core.replaceScene(createGameOverScene(board.result));

                remainingTime = (END_TIME - core.frame / core.fps).toFixed(2);
                timeField.text = '残り時間：' + remainingTime + '分';

                if(board.result.score > displayedScore) displayedScore += 12;
                scoreField.text = 'スコア　：' + displayedScore;
            });

            return scene;
        };

        /**
          * チュートリアルシーン
          */
        var createTutorialScene = function() {
            var scene = new Scene();

            var displayedScore = 0;

            var board = new Board(14, 60);

            var bgObj = new Product(scene.width, scene.height, LABO_PLUS_BG_IMG);
            scene.addChild(bgObj);

            var girl = new Human(80, 185, GIRL_IMG, scene.width, scene.height - 200);
            girl.tl.moveBy(-100, 0, FPS);

            var textField = new TextField(TUTORIAL_TEXT, 220, 90, 10, scene.height - 90);

            bgm.stop();

            scene.addChild(timeField);
            scene.addChild(scoreField);

            scene.addChild(board);
            scene.addChild(girl);
            scene.addChild(textField);

            scene.addChild(exitButton);

            timeField.text = '残り時間：∞分';
            scoreField.text = 'スコア　：****';

            textField.on('touchstart', function(){
                core.assets[PUSH_BUTTON_SE].clone().play();
                if(textField.textFrame == 4)
                    if(!board.puyoCnt) textField.textFrame--;
            });

            return scene;
        };

        /**
          * ゲームオーバーシーン
          */
        var createGameOverScene = function(result) {
            var scene = new Scene();

            scene.backgroundColor = 'gray';
            
            var resultViewer = new ResultViewer(result, 0, 30);
            resultViewer.tl.moveTo(30, 30, FPS);

            var bgObj = new Product(scene.width, scene.height, LABO_PLUS_BG_IMG);
            scene.addChild(bgObj);

            scene.addChild(exitButton);
            scene.addChild(resultViewer);

            return scene;
        };

        core.replaceScene(createTitleScene());

    }
    core.start();

};

function rand(n) {
    return Math.floor(Math.random() * n);
}
