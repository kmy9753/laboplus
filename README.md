# LaboPlus

留年が確定しそうな"あなた君"，  
あなた君をなんとかして助けたい"幼馴染ちゃん"の願いに，天の声が応える．  
単位をつなげて必修単位を取り戻そう！  


備考
------
#### URL
https://dl.dropboxusercontent.com/u/67010278/LaboPlus/index.html


プレイ画面
--------

|||
|---|---|
|![](https://dl.dropboxusercontent.com/u/67010278/screenshot/LaboPlus/ss_title.jpg)|![](https://dl.dropboxusercontent.com/u/67010278/screenshot/LaboPlus/ss_story.jpg)|
|![](https://dl.dropboxusercontent.com/u/67010278/screenshot/LaboPlus/ss_game.jpg)|![](https://dl.dropboxusercontent.com/u/67010278/screenshot/LaboPlus/ss_result.jpg)|